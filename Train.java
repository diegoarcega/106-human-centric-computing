import java.awt.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.event.*;

public class Train extends JFrame implements ActionListener {
	Font fontStyle = new Font("SansSerif", Font.BOLD, 25);
	
	/*initialise buttons */
	JButton BOpenDoors = new JButton("Open Doors");
	JButton BCloseDoors = new JButton("Close Doors");
	JButton BGo = new JButton("Go");
	JButton BStop = new JButton("Stop");
	JButton BStart = new JButton("Start Train");
	JButton BShutdown = new JButton("Shutdown Train");
	
	/*initialise general status */
	JLabel trainOn = new JLabel("Train Power: ");
	JTextField trainOnStatus = new JTextField("OFF");
	JLabel trainMove = new JLabel("Moving: ");
	JTextField trainMoveStatus = new JTextField("NO");
	JLabel doorsOpen = new JLabel("Doors: ");
	JTextField doorsOpenStatus = new JTextField("CLOSED");
	JLabel trainReady = new JLabel("Ready: ");
	JTextField trainReadyStatus = new JTextField("NO");
	
	/*initialise main display */
	JTextField station = new JTextField("Darlington"); 
	JTextField destination = new JTextField("Bishop Auckland");
	
	boolean returning = false;
	
	
	public Train()
	{
	   super("The Train Control System 2");
	   buildGUI();
	   pack();
	   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   setVisible(true);
	   
	   /*start the program with the required conditions */
	   BShutdown.setEnabled(false);
	   BStop.setEnabled(false);
	   BGo.setEnabled(false);
	   BCloseDoors.setEnabled(false);
	   BOpenDoors.setEnabled(false);
	}
 
	private void buildGUI()
	{
		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildMiddlePanel(), BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
	}
	
	/* State checker function to check each state when an event of button press happens */ 
	private void stateChecker(){
	    if(doorsOpenStatus.getText().equals("CLOSED") )
			BCloseDoors.setEnabled(false);
	    if(trainOnStatus.getText().equals("ON") && doorsOpenStatus.getText().equals("CLOSED") ){
	    	BGo.setEnabled(true);
	    }
	    if(doorsOpenStatus.getText().equals("OPEN") )
			BOpenDoors.setEnabled(false);
	    if(trainReadyStatus.getText().equals("NO") )
	    	BGo.setEnabled(false);
	    if(trainMoveStatus.getText().equals("NO") )
	    	BStop.setEnabled(false);
	    if(trainMoveStatus.getText().equals("YES") ){
	    	BStop.setEnabled(true);
	    	BGo.setEnabled(false);
	    	BOpenDoors.setEnabled(false);
	    }
	}
	
	/* Shutting down the train in 2 conditions */
	private void shutDownTrain(){
   	 	trainOnStatus.setText("OFF");
   	 	BShutdown.setEnabled(false);
		 BStop.setEnabled(false);
		 BGo.setEnabled(false);
		 BCloseDoors.setEnabled(false);
		 BOpenDoors.setEnabled(false);
		 BStart.setEnabled(true);
		 trainReadyStatus.setText("NO");
	}

	/* Core function to manage the change of stations 
	 * interprets whether the train is going or returning from the journey 
	 * Utilises the "returning" variable */
	private void changeStation(String currentStation_, Boolean returning){
		
		if(returning == false){
			if(currentStation_.equals("Darlington")){
				station.setText("North Road"); 
			}
			if(currentStation_.equals("North Road")){
				station.setText("Heighington"); 
			}
			if(currentStation_.equals("Heighington")){
				station.setText("Shildon"); 
			}
			if(currentStation_.equals("Shildon")){
				station.setText("Newton Aycliffe"); 
			}
			if(currentStation_.equals("Newton Aycliffe")){
				station.setText("Bishop Auckland"); 
				destination.setText("Darlington");
			}
		}else{
			if(currentStation_.equals("Bishop Auckland")){
				station.setText("Newton Aycliffe");
			}
			
			if(currentStation_.equals("North Road")){
				station.setText("Darlington");
				destination.setText("Bishop Auckland");
			}
			if(currentStation_.equals("Heighington")){
				station.setText("North Road"); 
			}
			if(currentStation_.equals("Shildon")){
				station.setText("Heighington"); 
			}
			if(currentStation_.equals("Newton Aycliffe")){
				station.setText("Shildon"); 
			}
			
		}	
	}
	
	
	 /* Defnining the Layouts 
      ------------- TOP Panel, for displaying the Station  */
    private JPanel buildTopPanel()
	{
		JPanel top = new JPanel();
		top.setLayout(new BorderLayout());
		station.setPreferredSize(new Dimension( 200, 60 ));
		station.setFont(fontStyle);
		station.setHorizontalAlignment(JTextField.CENTER);
		top.setBorder(new TitledBorder(new EtchedBorder(),	"Station: "));
        top.add(station,BorderLayout.NORTH);
		
		return top;
	}
	
	/* ------------- MIDDLE Panel, displaying the Train Destination */
	private JPanel buildMiddlePanel()
	{		
		JPanel middle = new JPanel();
		middle.setLayout(new BorderLayout());
				 	
		destination.setPreferredSize(new Dimension( 200, 60 ));
		destination.setFont(fontStyle);
		destination.setHorizontalAlignment(JTextField.CENTER);
		
		middle.setBorder(new TitledBorder(new EtchedBorder(),	"Train Destination: "));

		middle.add(destination,BorderLayout.CENTER);
		
		return middle;
	}
	
    /* ------------- BOTTOM Panel, displaying status and control buttons */
	private JPanel buildBottomPanel()
	{
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(1,2));
    	/* WEST panel: status */
    	JPanel bottomWest = new JPanel();
    	bottomWest.setLayout(new GridLayout(2,4));
        bottomWest.setPreferredSize(new Dimension(310, 9));
    	
        /* visual adjustments */
        doorsOpenStatus.setBackground(Color.LIGHT_GRAY);
        trainMoveStatus.setBackground(Color.LIGHT_GRAY);
    	trainOnStatus.setBackground(Color.LIGHT_GRAY);
    	trainReadyStatus.setOpaque(false);
    	trainMoveStatus.setOpaque(false);
    	doorsOpenStatus.setOpaque(false);
    	trainOnStatus.setOpaque(false);
    	trainReadyStatus.setBorder(javax.swing.BorderFactory.createEmptyBorder());
    	trainOnStatus.setBorder(javax.swing.BorderFactory.createEmptyBorder());
    	trainMoveStatus.setBorder(javax.swing.BorderFactory.createEmptyBorder());
    	doorsOpenStatus.setBorder(javax.swing.BorderFactory.createEmptyBorder());
    	/* adding to the west panel */
        bottomWest.add(trainOn);
    	bottomWest.add(trainOnStatus);
    	bottomWest.add(trainMove);
    	bottomWest.add(trainMoveStatus);
    	bottomWest.add(doorsOpen);
    	bottomWest.add(doorsOpenStatus);
    	bottomWest.add(trainReady);
    	bottomWest.add(trainReadyStatus);
    	bottom.add(bottomWest,BorderLayout.WEST);
    	
        /* EAST panel: control buttons */
    	JPanel bottomEast = new JPanel();
    	bottomEast.setLayout(new GridLayout(3,2));
    	
    	bottomEast.add(BCloseDoors);
    	bottomEast.add(BOpenDoors);
    	bottomEast.add(BStop);
    	bottomEast.add(BGo);
    	bottomEast.add(BShutdown);
    	bottomEast.add(BStart);
    	bottom.add(bottomEast,BorderLayout.EAST);

    	BOpenDoors.addActionListener(this);
    	BCloseDoors.addActionListener(this);
    	BGo.addActionListener(this);
    	BStop.addActionListener(this);
    	BStart.addActionListener(this);
    	BShutdown.addActionListener(this);
    	
		return bottom;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	     Object source = e.getSource();
	     
	     if(source == BOpenDoors){
	    	 doorsOpenStatus.setText("OPEN");
	     	 BOpenDoors.setEnabled(false);
	     	 BCloseDoors.setEnabled(true);
	     	 BShutdown.setEnabled(false);
	     }
	     else if(source == BCloseDoors){
	    	 doorsOpenStatus.setText("CLOSED");
	    	 trainReadyStatus.setText("YES");
	     	 BGo.setEnabled(true);
	     	 BShutdown.setEnabled(true);
	     	 }
	     else if(source == BGo){
	    	 trainMoveStatus.setText("YES");
	     	 BShutdown.setEnabled(false); 
	     	 changeStation(station.getText(), returning);
	     }
	     else if(source == BStop){
	    	 trainMoveStatus.setText("NO");
	    	 trainReadyStatus.setText("NO");
	    	 BOpenDoors.setEnabled(true);
	    	 BGo.setEnabled(false);
	    	 BShutdown.setEnabled(true);
	    	 if(station.getText().equals("Bishop Auckland")){
	    		 returning = true;
	    		 shutDownTrain();
	    	 }
	    	 if(station.getText().equals("Darlington")){
	    		 returning = false;
	    		 shutDownTrain();
	    	 }
	     }
	     else if(source == BStart){
	    	 trainOnStatus.setText("ON");
			 BShutdown.setEnabled(true);
			 BStop.setEnabled(true);
			 BGo.setEnabled(true);
			 BCloseDoors.setEnabled(false);
			 BOpenDoors.setEnabled(true);
			 BStart.setEnabled(false);
	     }
	     else if(source == BShutdown){
	    	 shutDownTrain();
	     }
	     
	     stateChecker();
	     repaint();
	}

	public static void main(String[] args) {
		Train trainControl = new Train();
	}
}